{
  if($1 == "$theme-color:"){
    print  "$theme-color:", c1, ";";
  }else if($1 == "$theme-color-complement:"){
    print "$theme-color-complement:", c2, ";"
  }else if($1 == "$theme-color-secondary:"){
    print "$theme-color-secondary:", c3, ";"
  }else if($1 == "$main-sidebar-color:"){
      print "$main-sidebar-color:", c4, ";"
  }else if($1 == "$control-sidebar-color:"){
      print "$control-sidebar-color:", c5, ";"
  }else if($1 == "$theme-circle-color:"){
      print "$theme-circle-color:", c6, ";"
  }else{
    print $0;
  }

}
