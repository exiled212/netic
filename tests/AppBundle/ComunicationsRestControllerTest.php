<?php

use GuzzleHttp\Client;


class ComunicationsRestControllerTest extends PHPUnit_Framework_TestCase
{


    public function testGetComunicationData()
    {

        $client = new Client([
            'base_uri' => 'http://127.0.0.1:8000',
            "defaults" => [
                'exceptions' => false,
            ]]);
        $id = 11;
        $response = $client->get("/api/comunicates/data", ["query" => [
            "user" => 22,
            "id" => $id
        ]]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey('ok', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('id', $data["data"]);
        $this->assertEquals($id, $data["data"]["id"]);
    }


}
