/*
 This is our Grunt Wrapper, this tells node we are using grunt!
 */

module.exports = function (grunt) {
    /*
     Load our Grunt Tasks
     */
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');


    /**
     * Another option to load the tasks is instead
     * use the plugin `load-grunt-tasks` and then
     * we can avoid the loadNpmTasks every time
     * and just use
     *  >> require('load-grunt-tasks')(grunt); <<
     */

    /*  Configure our tasks
     */
    grunt.initConfig({
        sass: {
            dist: {
                src: "web/assets/scss/styles.scss",
                dest: "web/assets/css/styles.css"
            },
            options: {sourceMap: true}
        },
        concat: {
            dist: {
                src: 'web/assets/src/js/*.js',
                dest: "web/assets/js/app.js"
            }
        }, watch: {
            css: {
                files: "web/assets/scss/**/*.scss",
                tasks: ['sass']
            }

        }
    });

    /*
     Register our tasks
     */

    grunt.registerTask('default', [
        'watch',
        'concat'
    ]);
};
