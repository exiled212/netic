<?php

namespace AdminBundle\Theme;


use Dmishh\SettingsBundle\Manager\SettingsManagerInterface;

class ThemeManager{



    /**
     * @var SettingsManagerInterface
     */
    private $settingsManager;

    private $circleColor;
    /**
     * ThemeManager constructor.
     */
    public function __construct(SettingsManagerInterface $settingsManager)
    {

        $this->settingsManager = $settingsManager;
    }

    /**
     * @return mixed
     */
    public function getThemeColor()
    {
        return $this->settingsManager->get("theme.color");
    }

    /**
     * @param mixed $themeColor
     */
    public function setThemeColor($themeColor)
    {
        $this->settingsManager->set("theme.color", $themeColor);
    }

    /**
     * @return mixed
     */
    public function getThemeColorComplement()
    {
        return $this->settingsManager->get("theme.colorComplement");
    }

    /**
     * @param mixed $themeColorComplement
     */
    public function setThemeColorComplement($color)
    {
        $this->settingsManager->set("theme.colorComplement", $color);
    }

    /**
     * @return mixed
     */
    public function getThemeColorSecondary()
    {

        return $this->settingsManager->get("theme.colorSecondary");

    }

    /**
     * @param mixed $themeColorSecondary
     */
    public function setThemeColorSecondary($themeColorSecondary)
    {
        $this->settingsManager->set("theme.colorSecondary", $themeColorSecondary);
    }

    /**
     * @return mixed
     */
    public function getCircleColor()
    {
        return $this->settingsManager->get("theme.circleColor");
    }

    /**
     * @param mixed $circleColor
     */
    public function setCircleColor($circleColor)
    {
             $this->settingsManager->set("theme.circleColor", $circleColor);
    }

    public function  setMainSidebarColor($color){
        $this->settingsManager->set("theme.mainSidebarColor", $color);
    }

    public function  getMainSidebarColor(){

        $this->settingsManager->get("theme.mainSidebarColor");
    }

    public function  setControlSidebarColor($color){

        $this->settingsManager->set("theme.controlSidebarColor", $color);
    }

    public function  getControlSidebarColor(){

        $this->settingsManager->get("theme.controlSidebarColor");
    }



}