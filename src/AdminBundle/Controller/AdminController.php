<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Activity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class AdminController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository(Activity::class);
        $activity = $repository->findAll();
        $tipoVista = "AdminIndex"; // Esta nueva variable permite filtrar las etiquetas de twig de forma que al cargar los componentes de de AdminController:index no se genere un error por no encontrar inicializada la variable $activity
        return $this->render('AdminBundle:Default:index.html.twig',["activity" => $activity, "tipoVista"=>$tipoVista] );
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/skin/change")
     */
    public function changeSkinAction(Request $request)
    {

        $data = json_decode($request->getContent(), true);
        if($data == null){
          //invalid json sent
          throw new HttpException();
        }
        $themeColor = $data["c1"];
        $themeComplementColor = $data["c2"];
        $themeSecondaryColor = $data["c3"];
        $themeMainSidebarColor = $data["c4"];
        $themeControlSidebarColor = $data["c5"];
        $themeCircleColor = $data["circleColor"];


        $this->get("theme_manager")->setThemeColor($themeColor);
        $this->get("theme_manager")->setThemeColorComplement($themeComplementColor);
        $this->get("theme_manager")->setThemeColorSecondary($themeSecondaryColor);
        $this->get("theme_manager")->setMainSidebarColor($themeMainSidebarColor);
        $this->get("theme_manager")->setControlSidebarColor($themeControlSidebarColor);
        $this->get("theme_manager")->setCircleColor($themeCircleColor);


        $awkScript = "colors.awk";
        $colorsFile = "assets/scss/_colors.scss";

        $script = 'awk -f ' . $awkScript . ' '.
            'c1="#' . $themeColor . '" '.
            'c2="#' . $themeComplementColor . '" '.
            'c3="#' . $themeSecondaryColor . '" '.
            'c4="#'. $themeMainSidebarColor . '" '.
            'c5="#'. $themeControlSidebarColor . '" '.
            'c6="#'. $themeCircleColor.'" ' .
            $colorsFile;

        exec($script . " > tmp && mv tmp " . $colorsFile);

        $activity = new Activity();
        $activity->setCategory(Activity::CATEG_THEME_UPDATE);
        $activity->setCreatedAt(new \DateTime());
        $activity->setDescription("Update theme colors");
        $em = $this->getDoctrine()->getManager();
        $em->persist($activity);
        $em->flush();

        return new JsonResponse($script. "Colors updated successfully");
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/logo/change")
     */
    public function changeLogoAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        // the image is sent as a base64 string
        $base64_string = $data["imageDataUrl"];
        //decode the string and save it
        $file = $this->base64_to_jpeg($base64_string, "assets/img/logo.png");


        $activity = new Activity();
        $activity->setCategory(Activity::CATEG_LOGO_UPDATE);
        $activity->setCreatedAt(new \DateTime());
        $activity->setDescription("Change site logo");
        $em = $this->getDoctrine()->getManager();
        $em->persist($activity);
        $em->flush();


        return new JsonResponse("ok", 201);
    }

    /**
     * Helper function to decode the image and save to file
     */

    private function base64_to_jpeg($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");

        $data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);

        return $output_file;
    }

}
