<?php

namespace AppBundle\Security;


use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class JwtTokenAuthenticator extends  AbstractGuardAuthenticator
{
    /**
     * @var JWTEncoderInterface
     */
    private $JWTEncoder;
    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * JwtTokenAuthenticator constructor.
     */
    public function __construct(JWTEncoderInterface $JWTEncoder, EntityManager $entityManager)
    {

        $this->JWTEncoder = $JWTEncoder;
        $this->entityManager = $entityManager;
    }

    public function getCredentials(Request $request)
    {
        // STEP 1
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );
        //get the token from the request
        $token = $extractor->extract($request);

        if(!$token){
            // Comment by krvajal
            // if there is no token, it will return null
            // this way the authentication process will stop
            // by stop it means it wont try to authenticate
            // this way
            return;
        }
        // return the token
        return $token;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        // STEP 2
        // the credentials string contains the token from step 1
        // decode the token
        $data = $this->JWTEncoder->decode($credentials);
        if($data == false){
            //the token expired or was tempered
            throw  new CustomUserMessageAuthenticationException("Invalid token");

        }

        $username = $data["username"];

        //find the user by the username
        return $this->entityManager->getRepository("AppBundle:User")->findOneBy(["username"=> $username]);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // STEP 3
        // if the user is found this method get called
        return true;

    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // TODO: Implement onAuthenticationFailure() method.
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // TODO: Implement onAuthenticationSuccess() method.
    }

    public function supportsRememberMe()
    {
        return false;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse([
            'error' => 'auth required'
        ], 401);
    }


}