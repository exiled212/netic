<?php

namespace AppBundle\Users;

use AppBundle\Comunications\ComunicationsManager;
use AppBundle\Entity\User;
use AppBundle\Entity\Department;
use Doctrine\ORM\EntityManager;

class UsersManager{

    protected $em;
    protected  $comunicationsManager;
    public function __construct(EntityManager $em, ComunicationsManager $comunicationsManager){

        $this->em =  $em;
        $this->comunicationsManager = $comunicationsManager;
    }
    public function deleteUser(User $user){

        $this->comunicationsManager->removeUserFromComunicates($user);
        $this->em->remove($user);
        $this->em->flush();

    }
    public  function  updateUser(User $user){

        $this->em->persist($user);
        $this->em->flush();
    }

    /**
     * @param Department $group
     */
    public  function  findUsersByGroup($group){


    }

    public function findUserByRole($roles)
    {

        $qb = $this->em->createQueryBuilder();

        $qb->select('u')
            ->from(User::class, 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"' . $roles . '"%');

        return $qb->getQuery()->getResult();

    }



}