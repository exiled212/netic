<?php

namespace AppBundle\Form;

use AppBundle\Entity\Address;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('line1', TextType::class, ["required" => false])
            ->add("city", TextType::class, ["required" => false])
            ->add("region", TextType::class, ["required" => false])
            ->add("country", CountryType::class, ['data' => "ES", "required" => false])
            ->add("zip_code", TextType::class, ["required" => false]);
//        dump(Intl::getRegionBundle()->getCountryNames());
//        die();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Address::class,
        ));
    }


}
