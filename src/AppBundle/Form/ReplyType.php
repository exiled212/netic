<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 3/10/17
 * Time: 11:01 AM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReplyType extends  AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder->add('content','textarea');

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ComunicationReply'
        ));
    }
    public function getName()
    {
       return "reply";
    }


}