<?php

namespace AppBundle\Form;

use AppBundle\Entity\GlobalSettings;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GlobalSettingsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('bossMessaging', CheckboxType::class, ["required" => false])
//            ->add("adminMessaging", CheckboxType::class, ["required" => false])
            ->add("employeeMessaging", CheckboxType::class, ["required" => false])
            ->add("managerToGroupMessaging", CheckboxType::class, ["required" => false])
            ->add("managerDirectMessages", CheckboxType::class, ["required" => false])
            ->add("save", SubmitType::class, array("label" => "save"));


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => GlobalSettings::class
        ));
    }


}