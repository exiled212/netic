<?php


namespace AppBundle\Api\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileBrowserController extends  Controller {



    /**
     * @Route("browser/browse/type/all", name="api_browse_all")
     */
    public function browseAllAction(){


    }

    /**
     * @Route("browser/upload/type/all", name="api_upload_all")
     */
    public  function uploadAllAction(){


    }

    /**
     * @Route("browser/browse/type/image", name="api_browse_image")
     */
    public  function  browseImageAction(){

        return new Response("");
    }

    /**
     * @Route("browser/upload/type/image", name="api_upload_image")
     * @param Request $request
     * @return Response
     */
    public function uploadImageAction(Request $request){

        $callback =  $request->get("CKEditorFuncNum");
        $CKEditor = $request->get("CKEditor");
        $file = $request->files->get('upload');


        $filename = $this->get("post_images_uploader")->upload($file);
        $base_url  = "/".$this->getParameter("post_rel_images_directories");
        $url = $base_url.$filename;



        return new Response("<html><body><script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(".$callback.", '".$url."');</script></body></html>");

    }

}