<?php


namespace AppBundle\Api\Controller;

use AppBundle\Entity\Attachment;
use AppBundle\Entity\Comunication;
use AppBundle\Entity\ComunicationReply;
use AppBundle\Entity\User;
use AppBundle\Entity\View;
use AppBundle\Entity\Department;
use Doctrine\DBAL\Types\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class ComunicationsRestController extends RestController
{


    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/upload",name="api_attachment_upload")
     */
    public function uploadAction(Request $request)
    {

        dump($request->getContent());

        $attachment = new Attachment();
        $form = $this->createForm($attachment)
            ->add("filename", FileType::class, ["mapped" => false])
            ->add("used_id", TextType::class, ["mapped" => false]);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $data = $form->getData();
        }
        return new JsonResponse(["status" => "ok"]);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/comunicates",name="api_comunicates_get")
     */
    public function comunicatesAction(Request $request)
    {


        if ($request->getMethod() == 'GET') {


            $user_id = intval($request->get("user"));
            $unread = intval($request->get("unread", 0));
           

            $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);


            $comunicates = [];
            if ($unread == 1) {
                $comunicates = $this->get("comunications.manager")->getUnreadGeneralComunicates($user);
            } else {
                $comunicates = $this->get("comunications.manager")->getGeneralComunicatesPaginator(1, 1000);

            }

            //serialize the comunicate now
            $comunicates_array = [];
            /** @var Comunication $comunicate */
            foreach ($comunicates as $comunicate) {

                $comunicates_array [] = $this->serializeComunicate($comunicate);
            }
            return new JsonResponse($comunicates_array);

        } else {

            throw new NotFoundHttpException("unsuported method");
        }
    }

    /**
     * receive a comunicate and return an array version
     * @param Comunication $message
     * @return array
     */
    public function serializeComunicate(Comunication $message)
    {

        return [
            "id" => $message->getId(),
            "title" => $message->getTitle(),
            "createdAt" => $message->getDate(),
            "content" => $message->getContent(),
            "author" => $this->getUserArrayData($message->getAuthor()),
            "acceptsReplies" => $message->getAcceptsReplies(),
            "type" => $message->getType(),
            "views" => $this->serializeMessageViews($message->getViews()),
            "attachments" => $this->serializeAttachements($message->getAttachments()),
            "replies" => $this->serializeReplies($message->getReplies()),
        ];

    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserArrayData($user)
    {

        $user_arr = [
            "id" => $user->getId(),
            "firstName" => $user->getFirstName(),
            "lastName" => $user->getLastName(),
            "profilePicUrl" => $this->getParameter("profiles_rel_directories") . $user->getAvatar()
        ];

        return $user_arr;
    }

    private function serializeMessageViews($views)
    {
        $arr = [];
        if ($views) {
            /** @var View $view */
            foreach ($views as $view) {
                $arr [] = ["user" => $this->serializeUser($view->getUser()), "date" => $view->getViewAt()];
            }
        }
        return $arr;
    }

    private function serializeAttachements($attachmentsList)
    {
        $arr = [];
        if ($attachmentsList) {
            foreach ($attachmentsList as $attachment) {
                $arr [] = ['id' => $attachment->getId(), 'path' => $attachment->getFilename(), "name" => $attachment->getSourceName()];
            }
        }
        return $arr;
    }

    private function serializeReplies($replies)
    {
        $repliesArr = [];
        if ($replies) {
            foreach ($replies as $reply) {
                $repliesArr [] = ["id" => $reply->getId(),
                    "author" => $this->serializeUser($reply->getAuthor()),
                    "content" => $reply->getContent(),
                    "createdAt" => $reply->getCreatedAt()];

            }
        }
        return $repliesArr;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/group/comunicates",name="api_group_comunicates_get")
     * @Method("GET")
     */
    public function groupComunicatesAction(Request $request)
    {


        if ($request->getMethod() == 'GET') {

            $user_id = intval($request->get("user"));

            $group_id = intval($request->get("group_id"));


            $unread = $request->get("unread", 0);

            $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);

            /** @var Department $group */
            $group = $this->getDoctrine()->getRepository(Department::class)->find($group_id);
            if ($unread == 1) {
                $comunicates = $this->get("comunications.manager")->getUnreadGroupComunicates($user, $group);
            } else {

                $comunicates = $this->get("comunications.manager")->getGroupComunicatesPaginator($user, $group, 1, 1000);
            }


            //serialize the comunicate now
            $comunicates_array = [];
            /** @var Comunication $comunicate */

            foreach ($comunicates as $comunicate) {

                $comunicates_array [] = $this->serializeComunicate($comunicate);
            }

            return new JsonResponse($comunicates_array);

        } else {

            throw new NotFoundHttpException("unsuported method");
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/user/comunicates",name="api_user_comunicates_get")
     * @Method("GET")
     */
    public function userComunicatesAction(Request $request)
    {

        if ($request->getMethod() == 'GET') {

            $user_id = intval($request->get("user"));

            $unread = $request->get("unread", 0);

            $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);

            /** @var Department $group */
            if ($unread == 1) {
                $comunicates = $this->get("comunications.manager")->getUnreadDirectComunicates($user);
            } else {
                $comunicates = $this->get('comunications.manager')->getDirectComunicatesPaginator($user, 1, 1000);
            }


            //serialize the comunicate now
            $comunicates_array = [];
            /** @var Comunication $comunicate */

            foreach ($comunicates as $comunicate) {

                $comunicates_array [] = ["id" => $comunicate->getId(),
                    "title" => $comunicate->getTitle(),
                    "createdAt" => $comunicate->getDate(),
                    "content" => $comunicate->getContent(),
                    "author" => $this->getUserArrayData($comunicate->getAuthor())
                ];
            }

            return new JsonResponse($comunicates_array);

        } else {

            throw new NotFoundHttpException("unsuported method");
        }
    }

    /**
     * @Route("/comunicates/data", name="api_comunicate_data")
     */
    public function comunicateDataAction(Request $request)
    {
        $comunicationsManager = $this->get("comunications.manager");
        if ($request->getMethod() == "GET") {
            $user_id = intval($request->get("user"));
            $com_id = intval($request->get("id"));


            $message = $this->getDoctrine()->getRepository(Comunication::class)->find($com_id);
            $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);

            if ($message && $user) {
                $comunicationsManager->addView($message, $user);

                $com_arr = $this->serializeComunicate($message);

                return new JsonResponse(["ok" => true, "data" => $com_arr]);
            } else {

                return new JsonResponse(["ok" => false, "error" => "Not valid comunicate found"]);
            }
        } else {

            return new NotFoundHttpException("invalid method");
        }

    }

    /**
     * @Route("/comunicates/send",name="api_comunicates_send")
     * @Method("POST")
     */
    public function sendComunicateAction(Request $request)
    {

        

            $data = $data = json_decode($request->getContent(), true);
            $user_id = $data['user'];
            $title = $data["title"];
            $content = $data["content"];
            $replies = $data["replies"];

            $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);
            if ($user == null) {

                return new JsonResponse(["ok" => false, "error" => "invalid user"]);
            }
            $comunicate = new Comunication();
            $comunicate->setType(Comunication::TYPE_GENERAL);
            $comunicate->setAuthor($user);
            $comunicate->setDate(new \DateTime());
            $comunicate->setTitle($title);
            $comunicate->setContent($content);
            $comunicate->setAcceptsReplies($replies == 1);


            $this->get('comunications.manager')->save($comunicate);


            $respone = new JsonResponse(["ok" => true, "data" => $this->serializeComunicate($comunicate)], 201);
            $respone->headers->set("Location", "/comunicates/data");
            return $respone;


    }

    /**
     * @Route("/group/comunicates/send",name="api_group_comunicates_send")
     */

    public function sendGroupComunicateAction(Request $request)
    {

        if ($request->getMethod() == "POST") {

            $data = $data = json_decode($request->getContent(), true);
            $user_id = $data['user'];
            $group_id = $data['group'];
            $title = $data["title"];
            $content = $data["content"];
            $replies = $data["replies"];
            $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);
            if ($user == null) {

                return new JsonResponse(["ok" => false, "error" => "invalid user"]);
            }
            $group = $this->getDoctrine()->getRepository(Department::class)->find($group_id);

            $comunicate = new Comunication();
            $comunicate->setType(Comunication::TYPE_GROUP);
            $comunicate->setWorkgroup($group);
            $comunicate->setAuthor($user);
            $comunicate->setDate(new \DateTime());
            $comunicate->setTitle($title);
            $comunicate->setContent($content);
            $comunicate->setAcceptsReplies($replies == 1);

            $this->get('comunications.manager')->save($comunicate);


            return new JsonResponse(["ok" => 1, "data" => $this->serializeComunicate($comunicate)]);

        }


    }

    /**
     * @Route("/users/comunicates/send",name="api_user_comunicates_send")
     */

    public function sendUsersComunicateAction(Request $request)
    {

        if ($request->getMethod() == "POST") {

            $data = $data = json_decode($request->getContent(), true);

            $user_id = $data["user"];
            $title = $data["title"];
            $content = $data["content"];
            $dest = $data["dest"];
            $replies = $data["replies"];

            $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);
            if ($user == null) {

                return new JsonResponse(["ok" => false, "error" => "invalid user"]);
            }

            $comunicate = new Comunication();
            $comunicate->setType(Comunication::TYPE_DIRECT);
            // $comunicate->setWorkgroup($group);
            $comunicate->setAuthor($user);
            $comunicate->setDate(new \DateTime());
            $comunicate->setTitle($title);
            $comunicate->setContent($content);
            $comunicate->setAcceptsReplies($replies == 1);

            foreach ($dest as $user_arr) {
                $user = $this->getDoctrine()->getRepository(User::class)->find($user_arr['id']);
                $comunicate->addUser($user);
            }


            $em = $this->getDoctrine()->getManager();
            $em->persist($comunicate);
            $em->flush();

            return new JsonResponse(["ok" => true, "data" => $this->serializeComunicate($comunicate)]);

        }


    }

    /**
     * @Route("/user/comunicates/reply",name="api_user_comunicates_reply")
     */
    public function replyAction(Request $request)
    {


        if ($request->getMethod() == "POST") {

            $data = json_decode($request->getContent(), true);
            $request->request->replace($data);
            $request = $request->request;
            $user_id = $request->get("user");
            $comunicate_id = $request->get("comunication_id");
            $reply_text = $request->get("reply");

            $comunicate = $this->getDoctrine()->getRepository(Comunication::class)->find($comunicate_id);


            $user = $this->getDoctrine()->getRepository(User::class)->find($user_id);
            $reply = new ComunicationReply();
            $reply->setAuthor($user);
            $reply->setCreatedAt(new \DateTime());
            $reply->setContent($reply_text);
            $reply->setComunication($comunicate);

            $comunicate->addReply($reply);


            //save the comunicate data
            $this->get('comunications.manager')->save($comunicate);


            return new JsonResponse(["ok" => true,
                "data" => [
                    "comunicate" => ["id" => $comunicate->getId()]
                ]
            ]);
        } else {

            return new JsonResponse(["ok" => false]);
        }

    }

}