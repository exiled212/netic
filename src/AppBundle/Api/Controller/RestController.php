<?php


namespace AppBundle\Api\Controller;

use AppBundle\Entity\Attachment;
use AppBundle\Entity\Comunication;
use AppBundle\Entity\ComunicationReply;
use AppBundle\Entity\User;
use AppBundle\Entity\Department;
use Doctrine\DBAL\Types\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\CallbackValidator;


class RestController extends Controller
{

      protected function serializeGroup($group){


        return [
                    "id" => $group->getId(),
                    "name"  => $group->getName()
            ];
    }

    private function serializeAddress($address){
        if($address){
             return
              [ "line1"=> $address->getLine1(),
                "region" => $address->getRegion(),
                "city" => $address->getCity(),
                "country" => $address->getCountry(),
                "zip_code" => $address->getZipCode()
                ];
        }else{
            return null;
        }

    }
     /**
     * @param $user
     * @return array
     */
    protected function serializeUser($user){

        $group = $user->getDepartment();

        $user_arr =  [
                    "id" => $user->getId(),
                    "firstName"  => $user->getFirstName(),
                    "lastName"   => $user->getLastName(),
                    "username" => $user->getUsernameCanonical(),

                    "group" =>[
                        "id" => $group->getId(),
                        "name" => $group->getName()
                    ],
                    "admin" => $user->getAdminRights(),
                    "manager" => $user->getGroupBoss(),
                    "email" => $user->getEmail()
                 ];
        $user_arr["permissions"]= $this->serializePermissions();
        if($user->getAvatar()){
            //the user has an avatar

            $user_arr["profilePicUrl"] =  $this->getParameter("profiles_rel_directories").$user->getAvatar();
        }else{

            $user_arr["profilePicUrl"] = "assets/img/dummy.png";
        }

        if($user->getAddress()){
            $user_arr["address"] =  $this->serializeAddress($user->getAddress());

        }
        return $user_arr;

    }

    protected function serializePermissions(){
        $perm_arr = [];
        $settings = $this->get("settings")->getSettings();
        $perm_arr["employee_messaging"] = $settings->getEmployeeMessaging();
        $perm_arr["admin_messaging"] = $settings->getAdminMessaging();
        $perm_arr["manager_to_group_messaging"] = $settings->getManagerToGroupMessaging();
        $perm_arr["manager_direct_messages"] = $settings->getManagerDirectMessages();
        return $perm_arr;
    }

}

