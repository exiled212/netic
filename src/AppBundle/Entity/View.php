<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class View represents a view of a message by one user
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="ViewRepository")
 * @ORM\Table(name="views")
 */
class View
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"all"})
     * @ORM\JoinColumn{onDelete="CASCADE"}
     */
    protected $user;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $viewAt;
    /**
     * @ORM\ManyToOne(targetEntity="Comunication",inversedBy="views", cascade={"all"})
     */
    protected $message;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get when
     *
     * @return \DateTime
     */
    public function getViewAt()
    {
        return $this->viewAt;
    }

    /**
     * Set when
     *
     * @param \DateTime $when
     *
     * @return View
     */
    public function setViewAt($when)
    {
        $this->viewAt = $when;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return View
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get message
     *
     * @return \AppBundle\Entity\Comunication
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set message
     *
     * @param \AppBundle\Entity\Comunication $message
     *
     * @return View
     */
    public function setMessage(\AppBundle\Entity\Comunication $message = null)
    {
        $this->message = $message;

        return $this;
    }
}
