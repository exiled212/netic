<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Attachment
 *
 * @ORM\Table(name="attachment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttachmentRepository")
 */
class Attachment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     * @Assert\File(
     *     maxSize = "50M"
     * )
     */
    private $filename;


    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $sourceName;
    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", length=255)
     */
    private $mime_type;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    private $enabled;

    /**
     * @ORM\Column(type="decimal")
     * @Assert\NotBlank()
     */
    private $size;

    /**
     * @ORM\ManyToOne(targetEntity="Comunication", inversedBy="replies")
     * @ORM\JoinColumn(name="comunication_id", referencedColumnName="id")
     */

//    /**
//     * @ORM\ManyToOne(targetEntity="Comunication", inversedBy="attachments")
//     * @ORM\JoinColumn(name="comunication_id",referencedColumnName="id")
//     */
//    private $comunication;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Attachment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Attachment
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $this->setFormat($ext);
        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set format
     *
     * @param string $format
     *
     * @return Attachment
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }


    /**
     * Get format
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Attachment
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set comunicate
     *
     * @param \AppBundle\Entity\Comunication $comunicate
     *
     * @return Attachment
     */
    public function setComunicate(\AppBundle\Entity\Comunication $comunicate = null)
    {
        $this->comunicate = $comunicate;

        return $this;
    }

    /**
     * Get comunicate
     *
     * @return \AppBundle\Entity\Comunication
     */
    public function getComunicate()
    {
        return $this->comunicate;
    }

    /**
     * Set comunication
     *
     * @param \AppBundle\Entity\Comunication $comunication
     *
     * @return Attachment
     */
    public function setComunication(\AppBundle\Entity\Comunication $comunication = null)
    {
        $this->comunication = $comunication;

        return $this;
    }

    /**
     * Get comunication
     *
     * @return \AppBundle\Entity\Comunication
     */
    public function getComunication()
    {
        return $this->comunication;
    }

    /**
     * Set sourceName
     *
     * @param string $sourceName
     *
     * @return Attachment
     */
    public function setSourceName($sourceName)
    {
        $this->sourceName = $sourceName;

        return $this;
    }

    /**
     * Get sourceName
     *
     * @return string
     */
    public function getSourceName()
    {
        return $this->sourceName;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     *
     * @return Attachment
     */
    public function setMimeType($mimeType)
    {
        $this->mime_type = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mime_type;
    }

    /**
     * Set size
     *
     * @param string $size
     *
     * @return Attachment
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return string
     */

    public function getSizeString()
    {
        $kiloByte = 1024;
        $megaByte = 1024 * $kiloByte;
        if ($this->size >= $megaByte) {
            $sizeInMegaBytes = $this->size / $megaByte;
            return number_format($sizeInMegaBytes, "2") . " Mb";
        } else if ($this->size >= $kiloByte) {
            $sizeInKiloBytes = $this->size / $megaByte;
            return number_format($sizeInKiloBytes, "2") . " Kb";
        } else {
            return number_format($this->size, 2) . " bytes";
        }
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }
}
