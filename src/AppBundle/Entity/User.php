<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping\AttributeOverride;
use Doctrine\ORM\Mapping\AttributeOverrides;
use Doctrine\ORM\Mapping\Column;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 * @UniqueEntity(
 *     fields={"employee_id"},
 *     message="Ya existe otro empleado con ese ID"
 * )
 * @AttributeOverrides({
 *      @AttributeOverride(name="email",
 *          column=@ORM\Column(
 *              name     = "email",
 *              nullable = true,
 *              unique   = false,
 *              type =  "string"
 *          )
 *      ),
 *      @AttributeOverride(name="emailCanonical",
 *          column=@ORM\Column(
 *              name     = "email_canonical",
 *              nullable = true,
 *              unique   = false,
 *              type =  "string"
 *          )
 *      )
 * })
 */
class User extends BaseUser
{

    const DEVICE_IOS = 1;
    const DEVICE_ANDROID = 2;
    const DEVICE_WP =  3;
    const DEVICE_WEB = 4;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\Column(type="string",nullable=true)
     *
     */
    protected $avatar;


    /**
     * @ORM\Column(type="string",nullable=true)
     * @Assert\NotBlank(message="Debe introducir un numero de empleado")
     */
    protected $employee_id;

    /**
     * @ORM\Column(type="text",nullable=true)
     *
     * @Assert\NotBlank(message="Debe introducir su nombre")
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */

    protected $firstname;
    /**
     * @ORM\Column(type="text",nullable=true)
     * @Assert\NotBlank(message="Debe introducir su apellido")
     * @Assert\Length(min=3)
     */
    protected $lastname;

    /**
     * @ORM\Column(type ="boolean")
     */
    protected $admin_rights = false;


    /**
     * @ORM\Column(type ="boolean")
     */
    protected $group_boss = false;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Department",inversedBy="members")
     * @ORM\JoinColumn(name="workgroup_id", referencedColumnName="id")
     */
    protected $department;

    //address data
    /**
     * @ORM\OneToOne(targetEntity="Address",cascade={"persist"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     *
     * @Assert\Type(type="AppBundle\Entity\Address")
     * @Assert\Valid()
     */
    protected $address;

    /**
     * @ORM\Column(type="text",nullable=true))
     */

    protected $phone;

    /**
     * @ORM\Column(type="text",nullable=true))
     */

    protected $mobile;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    protected $birthdate;


    /**
     * @ORM\Column(type="text",nullable=true)
     */
    protected  $device_token;


    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Choice(
     *     choices = {User::DEVICE_IOS,
     *                User::DEVICE_ANDROID,
     *                User::DEVICE_WEB,
     *                User::DEVICE_WP})
     */
    protected  $device_type;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Get workgroup
     *
     * @return \AppBundle\Entity\Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set workgroup
     *
     * @param Department $department
     * @return User
     * @internal param Department $workgroup
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get admin_rights
     *
     * @return boolean
     */
    public function getAdminRights()
    {
        return $this->admin_rights;
    }

    /**
     * Set admin_rights
     *
     * @param boolean $adminRights
     * @return User
     */
    public function setAdminRights($adminRights)
    {
        $this->admin_rights = $adminRights;

        return $this;
    }

    /**
     * Get group_boss
     *
     * @return boolean
     */
    public function isGroupBoss()
    {
        return $this->group_boss;
    }

    /**
     * Get first_name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstname;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstname = $firstName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastname;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastname = $lastName;

        return $this;
    }

    /**
     * Get employee_id
     *
     * @return string
     */
    public function getEmployeeId()
    {
        return $this->employee_id;
    }

    /**
     * Set employee_id
     *
     * @param string $employeeId
     * @return User
     */
    public function setEmployeeId($employeeId)
    {
        $this->employee_id = $employeeId;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\Address $address
     * @return User
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return User
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     * @return User
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getFullName()
    {
        return $this->firstname . " " . $this->lastname;
    }

    /**
     * Get deviceToken
     *
     * @return string
     */
    public function getDeviceToken()
    {
        return $this->device_token;
    }

    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     *
     * @return User
     */
    public function setDeviceToken($deviceToken)
    {
        $this->device_token = $deviceToken;

        return $this;
    }

    /**
     * Get deviceType
     *
     * @return integer
     */
    public function getDeviceType()
    {
        return $this->device_type;
    }

    /**
     * Set deviceType
     *
     * @param integer $deviceType
     *
     * @return User
     */
    public function setDeviceType($deviceType)
    {
        $this->device_type = $deviceType;

        return $this;
    }

    /**
     * Get groupBoss
     *
     * @return boolean
     */
    public function getGroupBoss()
    {
        return $this->group_boss;
    }

    /**
     * Set group_boss
     *
     * @param boolean $groupBoss
     * @return User
     */
    public function setGroupBoss($groupBoss)
    {
        $this->group_boss = $groupBoss;

        return $this;
    }
}
