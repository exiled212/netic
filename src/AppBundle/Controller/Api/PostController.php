<?php

namespace AppBundle\Controller\Api;


use AppBundle\Entity\Comunication;
use AppBundle\Entity\Department;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

// *
/**
 * Class PostController
 * @package AppBundle\Controller\Api
 */
class PostController extends ApiController
{

    /**
     * @Route("/posts",name="api_v2_posts_collection")
     * @Method("GET")
     */
    public function listAction(Request $request)
    {

        $filter = $request->query->get('filter');

        $unread = $request->query->getBoolean("unread", false);
        $type = $request->query->getInt("type");
        $options = [];
        if ($filter) {
            $options["filter"] = $filter;
        }
        $user = $this->getUser();

        $options["unread"] = $unread;


        if ($type == 3) {
            //direct message
            $user = $this->getUserFromQueryParams($request);
            $qb = $this->getDoctrine()->getRepository("AppBundle:Comunication")->findDirectQueryBuilder($user, $options);

        } else if ($type == 2) {

            $department = $this->getDepartmentFromQueryParams($request);

            $qb = $this->getDoctrine()->getRepository("AppBundle:Comunication")->findInDepartmentQueryBuilder($department, $options, $user);
        } else {
            $qb = $this->getDoctrine()->getRepository("AppBundle:Comunication")->findAllQueryBuilder($options, $user);
        }


        $paginatedCollection = $this->get('pagination_factory')
            ->createCollection($qb, $request, 'api_v2_posts_collection');


        $response = $this->createApiResponse($paginatedCollection, 200);

        return $response;
    }

    /**
     * @param Request $request
     * @return null| User
     */
    protected function getUserFromQueryParams(Request $request)
    {
        $user_id = $request->query->get("user");
        $user = $this->get('doctrine')->getRepository("AppBundle:User")->find($user_id);

        if (!$user) {
            throw $this->createNotFoundException(sprintf(
                'No user found with id "%d"', $user_id
            ));
        }
        return $user;
    }

    /**
     * @param Request $request
     * @return \AppBundle\Entity\Department|null
     */
    protected function getDepartmentFromQueryParams(Request $request)
    {
        $department_id = $request->query->get("department");


        $department = $this->getDoctrine()->getRepository("AppBundle:Department")->find($department_id);

        if (!$department) {
            throw $this->createNotFoundException(sprintf(
                'No department found with id "%d"', $department_id
            ));
        }
        return $department;
    }

    /**
     * @Route("/posts")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        $data = $data = json_decode($request->getContent(), true);
        $post = new Comunication();

        $form = $this->createForm(new PostType(), $post);
        $form->submit($data);
        if ($form->isValid()) {

            $user = $this->getUser();

            $post->setDate(new \DateTime());
            $post->setAuthor($user);

            if($post->getType() == Comunication::TYPE_GROUP){
                $depId = $data['department'];
                /** @var Department $department */
                $department = $this->getDoctrine()->getRepository('AppBundle:Department')->find($depId);
                $post->setWorkgroup($department);

            }else if( $post->getType() == Comunication::TYPE_DIRECT){

                $usersIds = $data['users'];
                $users = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(["id"=> $usersIds]);
                /** @var User $user */
                foreach ($users as $user) {
                    $post->addUser($user);
                }
            }

            $this->get('comunications.manager')->save($post);

            //create the response
            $response = $this->createApiResponse($post,201);
            $postUrl = $this->generateUrl("api_v2_post_show", ["id" => $post->getId()]);
            $response->headers->set('Location', $postUrl);
            return $response;

        } else {
                // form has errors
          return $this->createValidationErrorResponse($form);

        }

    }

    /**
     * @Route("/posts/{id}", name="api_v2_post_show")
     * @Method("GET")
     */

    public function showAction($id)
    {

        $post = $this->getDoctrine()->getRepository('AppBundle:Comunication')->find($id);

        $user = $this->getUser();

        // add the view for the user
        $comunicationsManager = $this->get("comunications.manager");
        $comunicationsManager->addView($post, $user);
        return $this->createApiResponse($post, 200);
    }

}
