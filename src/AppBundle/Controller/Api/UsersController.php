<?php

namespace AppBundle\Controller\Api;


use AppBundle\Entity\User;
use AppBundle\Form\EmployeeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

//
///**
// * @Security("is_granted('ROLE_USER')")
// */
class UsersController extends ApiController
{


    /**
     * Create an new user
     * @Route("/users")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $user = new User();
        $form = $this->createForm(new EmployeeType(), $user);
        $form->submit($data);

        $user->setEnabled(true);
        $manager = $this->get("fos_user.user_manager");
        $manager->updateUser($user, true);

        $url = $this->generateUrl("api_v2_user_show", ["username" => $user->getUsername()]);
        $response = $this->createApiResponse($data, 201);

        $response->headers->set("Location", $url);
        return $response;
    }

    /**
     * @Route("/users",name="api_v2_user_list")
     * @Method("GET")
     */
    public function listAction(Request $request)
    {

        $page = $request->query->getInt("page", 1);
        $filter = $request->query->get("filter");
        $route = "api_v2_user_list";

        $qb = $this->getDoctrine()->getRepository("AppBundle:User")->findAllQueryBuilder($filter);

        $paginatedCollection = $this->get("pagination_factory")->createCollection($qb, $request, $route);

        return $this->createApiResponse($paginatedCollection);
    }

    /**
     * @Route("/users/{username}",name="api_v2_user_show")
     * @Method("GET")
     */
    public function showAction($username)
    {

        $user = $this->get("fos_user.user_manager")->findUserByUsername($username);

        return $this->createApiResponse($user, 200);
    }

    /**
     * @Route("/users/{username}")
     * @Method("PATCH")
     */
    public function updateAction($username, Request $request)
    {

        /** @var User $user */
        $user = $this->get("fos_user.user_manager")->findUserByUsername($username);

        if (!$user) {
            throw $this->createNotFoundException(sprintf(
                'No user found with username "%s"',
                $username
            ));
        }

        $manager = $this->container->get('fos_user.user_manager');

        $data = json_decode($request->getContent(), true);

        $form = $this->createForm(new EmployeeType(), $user);
        $form->submit($data);

        if ($user->isGroupBoss()) {
            $user->addRole("ROLE_MANAGER");
        } else {

            $user->removeRole("ROLE_MANAGER");
        }

        if ($user->getAdminRights() == true) {
            $user->addRole("ROLE_ADMIN");
        } else {
            $user->removeRole("ROLE_ADMIN");
        }

        $manager->updateUser($user);
        return $this->createApiResponse($user, 200);

    }

     /**
     * @Route("/users/avatar/upload",name="api_user_avatar_upload")
     * @Method("POST")
     */
    public function uploadAvatar(Request $request)
    {
        $id = $request->request->get("id");

        $user = $this->getUserById($id);

        $avatarFile = $request->files->get("avatar");

        if($avatarFile->isValid()){

            $avatar = $this->get("avatar_uploader")->upload($avatarFile);
            $user->setAvatar($avatar);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return  $this->createApiResponse($user,200);

        }else{
            dump($avatarFile->getErrorMessage());
            die();

            return new NotFoundHttpException($avatarFile->getErrorMessage());
        }

    }
    private function getUserById($user_id)
    {

        $user = $this->getDoctrine()->getRepository(User::class)->find(intval($user_id));

        return $user;
    }
}
