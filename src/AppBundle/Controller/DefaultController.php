<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comunication;
use AppBundle\Entity\Locale;
use AppBundle\Entity\User;
use AppBundle\Entity\Department;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        $comunicates = $this->get("comunications.manager")->getUnreadGeneralComunicates($this->getUser());
        $group_comunicates = $this->get("comunications.manager")->getUnreadGroupComunicates($this->getUser(),
            $this->getUser()->getDepartment());
        $direct_comunicates = $this->get("comunications.manager")->getUnreadDirectComunicates($this->getUser());


        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
            "comunicates" => $comunicates,
            "group_comunicates" => $group_comunicates,
            "direct_comunicates" => $direct_comunicates
        ));


    }

    public function sidebarAction()
    {


        $departments = $this->getDoctrine()->getRepository(Department::class)->findAll();

        $usercount = $this->getUserCount();

        $settings = $this->get('settings')->getSettings();

        /** @var User $user */
        $user = $this->getUser();
        $showManagerIcon = ($user->isGroupBoss() && $settings->getBossMessaging()) || $user->getAdminRights();
        $showOtherUsersIcon = $settings->getEmployeeMessaging() || $user->getAdminRights();
        $showUsersIcon = ($settings->getManagerDirectMessages() && $user->isGroupBoss()) || $user->getAdminRights();


        $showSelfGroupMessagingIcon = ($user->isGroupBoss() && $settings->getManagerToGroupMessaging()) || $user->getAdminRights();

        $showOthersGroupMessagingIcon = $user->getAdminRights();

        return $this->render("default/components/sidebar.html.twig", [
            "departments" => $departments,
            "usercount" => $usercount,
            "showManagerIcon" => $showManagerIcon,
            "showUsersIcon" => $showUsersIcon,
            "showOtherUsersIcon" => $showOtherUsersIcon,
            "showSelfGroupMessagingIcon" => $showSelfGroupMessagingIcon,
            "showOthersGroupMessagingIcon" => $showOthersGroupMessagingIcon
        ]);

    }

    /**
     * @return integer
     */
    function getUserCount()
    {

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb->select('count(user.id)');
        $qb->from('AppBundle:User', 'user');
        $count = $qb->getQuery()->getSingleScalarResult();
        return $count;
    }

    public function controlSidebarAction($general_com_page = 1, $direct_com_page =  1, $group_com_page = 1)
    {


        if($direct_com_page == null) $direct_com_page = 1;
        if($general_com_page == null) $general_com_page = 1;
        if($group_com_page == null) $group_com_page = 1;

        $general_comunications = $this->get("comunications.manager")->getGeneralComunicatesPaginator($general_com_page,5);

        $group_comunications = $this->get("comunications.manager")->getGroupComunicatesPaginator($this->getUser(), $this->getUser()->getDepartment(), $group_com_page, 5);

        $direct_comunications = $this->get('comunications.manager')->getDirectComunicatesPaginator($this->getUser(),$direct_com_page, 5);

        return $this->render("default/components/control-sidebar.html.twig", [
            "comunications" => $general_comunications,
            "group_comunications" => $group_comunications,
            "direct_comunications" => $direct_comunications,
        ]);

    }

    /**
     * @Route("/settings/save", name="save_settings")
     */
    public function settingsUpdateAction(Request $request)
    {

        $form = $this->get("settings")->settingsForm();

        $referer = $request->headers->get('referer');

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $settings = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($settings);
            $em->flush();
        }
        return $this->redirect($referer);

    }

    public function settingsAction(Request $originalRequest)
    {
        $form = $this->get("settings")->settingsForm();


        return $this->render(":default/components:permissions-form.html.twig", ["form" => $form->createView()]);

    }

    public function showLanguagesAction($mainRequest)
    {


        $locales = $this->getDoctrine()->getRepository(Locale::class)->findAll();

        return $this->render("default/components/locales.html.twig", ["locales" => $locales, "mainRequest" => $mainRequest]);

    }

    public function showNavbarAction($mainRequest)
    {

        return $this->render("default/components/navbar.html.twig", ["mainRequest" => $mainRequest]);
    }

}
