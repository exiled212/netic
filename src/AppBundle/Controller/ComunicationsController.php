<?php

namespace AppBundle\Controller;

use AppBundle\Comunications\ComunicationsManager;
use AppBundle\Entity\Attachment;
use AppBundle\Entity\Comunication;
use AppBundle\Entity\ComunicationReply;
use AppBundle\Entity\User;
use AppBundle\Entity\Department;
use AppBundle\Form\ComunicationType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Twig\Extension\RoutingExtension;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ComunicationsController extends Controller
{
    /**
     * @Route("/comunications/list", name="comunications")
     */
    public function listAction(Request $request)
    {
        //get all comunications


        $comunications = $this->getDoctrine()->getRepository(Comunication::class)->findBy([], ['date' => 'desc']);

        // replace this example code with whatever you need

        return $this->render('default/comunications.html.twig', [
                "comunications" => $comunications
            ]
        );
    }


    /**
     * @Route("/comunications/show/{id}", name="comunications_show")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $message = $this->getDoctrine()->getRepository(Comunication::class)->find($id);
        /** @var ComunicationsManager $comunicationsManager */
        $comunicationsManager = $this->get("comunications.manager");
        if ($message) {


            $comunicationsManager->addView($message, $this->getUser());

            if ($message->getAcceptsReplies()) {

                $form = $this->getReplyForm();


                return $this->render("default/comunications/show.html.twig", ["item" => $message, "form" => $form->createView()]);
            }

            return $this->render("default/comunications/show.html.twig", ["item" => $message]);
        } else {
            throw new NotFoundHttpException("Comunication not found with id " . $id);
        }
    }

    /**
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    public function getReplyForm()
    {
        $reply = new ComunicationReply();
        $form = $this->createFormBuilder($reply)
            ->add("content", TextType::class)
            ->getForm();
        return $form;
    }

    /**
     * @Route("/comunications/show/{id}/views", name="comunications_show_view")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showViewsAction($id){
        /** @var Comunication $comunication */
        $comunication = $this->getDoctrine()->getRepository(Comunication::class)->find($id);
        if($comunication){
            $views = $comunication->getViews();

            return $this->render("default/comunications/views.html.twig",[
                "item" => $comunication,
                "views" => $views
            ]);
        }else{
            throw new NotFoundHttpException("page not found for comunicate");
        }


    }

    /**
     * @Route("/comunications/create", name="comunications_create")
     */
    public function createAction(Request $request)
    {


        $comunication = new Comunication();

        $form = $this->createForm(ComunicationType::class, $comunication, ["locale" => $request->getLocale()]);


        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $this->processComunicateForm($form, Comunication::TYPE_GENERAL);

            return $this->redirectToRoute("comunications");

        }
        return $this->render('default/comunications/create.html.twig',
            ["form" => $form->createView()]
        );


    }

    /**
     * @param $form
     */
    protected function processComunicateForm($form, $type)
    {
        /** @var Comunication $comunication */
        $comunication = $form->getData();
        $attachments = $comunication->getAttachments();

        if ($attachments) {
            /** @var Attachment $item */
            foreach ($attachments as $item) {

                /** @var UploadedFile $file */
                $file = $item->getFileName();
                $item->setSourceName($file->getClientOriginalName());
                $item->setMimeType($file->getMimeType());
                $item->setSize($file->getSize());
                $realFilename = $this->get('attachment_uploader')->upload($file);
//                $type = $file->guessExtension();
                $item->setFilename($this->getParameter("attachments_rel_directories") . '/' . $realFilename);
                $item->setCreatedAt(new \DateTime());
                $item->setEnabled(true);


            }
        }


        $comunication->setDate(new \DateTime());
        $comunication->setAuthor($this->getUser());
        $comunication->setType($type);
        $comunication->setContent(str_replace("\n", "", $comunication->getContent()));


        $this->get('comunications.manager')->save($comunication);

    }

    /**
     * @Route("/comunications/workgroup/{groupid}/create", name="comunications_workgroup_create")
     */
    public function createWorkgroupComunicateAction($groupid, Request $request)
    {

        $workgroup = $this->getDoctrine()->getEntityManager()->getRepository(Department::class)->find($groupid);
        if ($workgroup) {

            $comunication = new Comunication();
            $comunication->setWorkgroup($workgroup);
            $form = $this->createForm(ComunicationType::class, $comunication, ["locale" => $request->getLocale()]);

            $form->add("department", TextType::class, ['label' => 'target_workgroup', "data" => $workgroup->getName(), "mapped" => false]);

            $form->add("acceptsReplies", CheckboxType::class, ["required" => false, "label" => "comunication.response.accept.label"]);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {


                $this->processComunicateForm($form, Comunication::TYPE_GROUP);

                return $this->redirectToRoute("homepage");

            } elseif ($form->isSubmitted()) {

                dump($form->getErrors(true));
                die();
            }
            return $this->render('default/comunications/group_comunicate_create.html.twig',
                ["form" => $form->createView()]
            );


        } else {

            throw  new NotFoundHttpException("Invalid workgroup");
        }


    }

    /**
     * @Route("/comunications/workgroup/{groupid}/users/create", name="comunications_workgroup_users_create")
     */
    public function createWorkgroupUsersComunicateAction($groupid, Request $request)
    {


        $workgroup = $this->getDoctrine()->getEntityManager()->getRepository(Department::class)->find($groupid);
        if ($workgroup) {

            $comunication = new Comunication();

            $users = $this->getDoctrine()->getRepository(User::class)->findBy(["department" => $workgroup]);

            $form = $this->createForm(ComunicationType::class, $comunication, ["locale" => $request->getLocale()]);

            $form->add("workgroup", TextType::class, ['label' => 'target_workgroup', "data" => $workgroup->getName(), "mapped" => false]);
            $form->add("users", EntityType::class,
                [   "label" => "users",
                    "choices" => $users,
                    "class" => User::class,
                    "multiple" => true,
                    "property" => "fullName"
                ]
            );
            $form->add("acceptsReplies", CheckboxType::class, ["required" => false, "label" => "comunication.response.accept.label"]);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $this->processComunicateForm($form, Comunication::TYPE_DIRECT);

                return $this->redirectToRoute("homepage");

            } elseif ($form->isSubmitted()) {

                dump($form->getErrors(true));
                die();
            }
            return $this->render('default/comunications/group_users_comunicate_create.html.twig',
                ["form" => $form->createView()]
            );


        } else {

            throw  new NotFoundHttpException("Invalid workgroup");
        }


    }


    /**
     * @Route("comunications/workgroup/{groupid}/manager/create", name="comunications_workgroup_manager_create")
     */

    public function createWorkgroupManagerComunicateAction($groupid, Request $request)
    {

        /** @var Department $group */
        $group = $this->getDoctrine()->getRepository(Department::class)->find($groupid);
        if ($group) {

            $comunication = new Comunication();
            /** @var User $manager */
            $manager = $group->getManager();
            if ($manager == null) {
                throw new NotFoundHttpException("The workgroup does not have a manager");
            }
            $comunication->addUser($manager);

            $comunication->setType(Comunication::TYPE_DIRECT);
            $form = $this->createForm(ComunicationType::class, $comunication, ["locale" => $request->getLocale()]);

            $form->add("workgroup", TextType::class, ['label' => 'target_workgroup', "data" => $group->getName(), "mapped" => false]);

            $form->add("acceptsReplies", CheckboxType::class, ["required" => false, "label" => "comunication.response.accept.label"]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $this->processComunicateForm($form, Comunication::TYPE_DIRECT);
                return $this->redirectToRoute("homepage");

            }

            return $this->render("default/comunications/group_manager_comunicate_create.html.twig", ["form" => $form->createView()]);


        }

    }

    /**
     * @Route("/comunications/{id}/reply/create", name="comunications_replay_create")
     */
    public function addReplyAction($id)
    {

        $comunication = $this->getDoctrine()->getRepository(Comunication::class)->find($id);
        if ($comunication) {

            if (!$comunication->getAcceptsReplies()) {

                return new NotFoundHttpException("Can't add reply to this comunicate");
            }

            $form = $this->getReplyForm();

            $form->handleRequest($this->get("request"));
            if ($form->isSubmitted() && $form->isValid()) {


                /** @var ComunicationReply $reply */
                $reply = $form->getData();
                $reply->setAuthor($this->getUser());
                $reply->setCreatedAt(new \DateTime());
                $reply->setComunication($comunication);

                $this->get("comunications.manager")->saveReply($comunication, $reply);


                return $this->redirectToRoute("comunications_show#reply_".$reply->getId(), ["id" => $id]);

            }
            return $this->redirectToRoute("comunications_show", ["id" => $id]);

        } else {

            throw new NotFoundHttpException("Comunication not found");
        }


    }


}
