<?php


namespace AppBundle\Tests\Controller\Api;


use AppBundle\Test\ApiTestCase;
use GuzzleHttp\Psr7\Response;


class TokenControllerTest extends ApiTestCase
{


    public function testPOSTCreateToken()
    {

        /** @var Response $response */
        $response = $this->client->post("/api/v2/auth/token", ["auth" => ["admin", "admin"]]);


        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);
        $this->asserter()->assertResponsePropertyExists($response,"token");

    }

    public function testPOSTTokenInvalidCredentials()
    {

        /** @var Response $response */
        $response = $this->client->post("/api/v2/auth/token", ["auth" => ["admin", "admin"]]);

        $this->assertEquals(401, $response->getStatusCode());

    }

}