<?php

namespace AppBundle\Tests\Controller\Api;


use AppBundle\Test\ApiTestCase;


class UsersControllerTest extends ApiTestCase
{


    public function testPostUser()
    {

        $data = [
            "username" => "admin",
            "firstName" => "Miguel",
            "lastName" => "Carvajal",
            "plainPassword" => "admin"
        ];
        $response = $this->client->post("/api/v2/users", [
            "body" => json_encode($data)
        ]);
        $this->assertEquals(201, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyExists($response, "firstName");
    }

    public function testPATCHUser()
    {

        $data = ["firstName" => "John",
//            'lastName' => "Doe",
//            "phone"=>"+5492944556741",
            "plainPassword" => "test",
            "address" => ["country" => "ES", "line1" => "line1"]];


        $response = $this->client->patch("/api/v2/users/admin", [
            "body" => json_encode($data),
            'headers' => $this->getAuthorizedHeaders("admin", [])
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyEquals($response, "firstName", "John");
//        $this->asserter()->assertResponsePropertyEquals($response, "lastName", "Doe");


    }
}
