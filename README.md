Source code for NETIC website.


# Technologies used? 
The website is built using Symgony 2.8 PHP framework.
Upon this we build the different controllers to display and handle the users and messages on NETIC.

### We use the following open source bundles

* Doctrine
* KnpPaginatorBundle
* FOS/UserBundle
* CKEditorBundle
* DoctrineFixturesBundle
* Nelmio/CorsBundle
* EightPoints/GuzzleBundle

As database layer is used MySQL database.

### How do I get set up? ###
To get setup you need to call
```
composer update
```
 to install the required bundles and then usen the symfony console to generate the database schemas
 ```
 php app/console doctrine:schema:update
 ```
 After that you will be ready to go
 
### Creating admin user
To create the admin user you shoud use the symfony command line commands provided by the FOSUserBundle
